from django.urls import path
from todos.views import show_list, detail_list, create_list, update_list
from todos.views import delete_list, create_item, update_item


urlpatterns = [
    path("", show_list, name="show_list"),
    path("<int:id>/", detail_list, name="detail_list"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", update_list, name="update_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("items/create/", create_item, name="create_item"),
    path("<int:id>/edit_item/", update_item, name="update_item")
]
