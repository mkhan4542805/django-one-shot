from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm
# Create your views here.


def show_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "items": todo_lists,
    }
    return render(request, "todos/list.html", context)


def detail_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    context = {
        "items": model_instance
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("detail_list", id=model_instance.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("detail_list", id=model_instance.id)
    else:
        form = TodoForm(instance=model_instance)

    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("show_list")

    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("detail_list", id=model_instance.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def update_item(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=list)
        if form.is_valid():
            model_instance = form.save()
            return redirect("detail_list", id=model_instance.list.id)
    else:
        form = ItemForm(instance=list)

    context = {
        "list_object": list,
        "item_form": form,
    }

    return render(request, "todos/edit_item.html", context)
